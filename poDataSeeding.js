const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;
const ObjectID = mongo.ObjectID;
var bodyParser = require('body-parser');
const uri = "mongodb+srv://storeInventoryTest:j6DR4zs11vG4rMMF@store-inventory-lower-q1vgj.azure.mongodb.net/storeInventoryTestDB?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true },{ useUnifiedTopology: true });

const payLoad = {
    "SupplyEvent": [{
        "LastTransDate": "2019-10-10T10:10:00.201Z",
        "TransactionTypeId": "POCreate",
        "SupplyDefinition": {
            "ReferenceId": null,
            "ItemId": "2785772",
            "LocationId": "100",
            "SupplyType": {
                "SupplyTypeId": "On Order"
            },
            "OriginLocationId": "3",
            "ReferenceDetailId": "Pals-100",
            "ReferenceTypeId": "PO",
            "SupplyData": {
                "ETA": "2019-11-11T11:11:00.201Z",
                "UOM": "EA",
                "Quantity": 3.0
            }
        }
    }]
}

var poData = [];
console.log("doc while adding to array:")
for(var i=0; i<3; i++) {
    payLoad.SupplyEvent[0].SupplyDefinition.ReferenceDetailId = "Pal-"+i;
    payLoad.number = "Pal-"+i;
    poData.push(payLoad);
    console.log(poData[i]);
}
console.log("===========-------------------------------------===========")
console.log("doc while retreving from array:")
poData.forEach((data) =>{
    console.log(data);
});

client.connect(err => {
    const collection = client.db("storeInventoryTestDB").collection("items");
    if (err) throw err;
    poData.forEach(function(doc)  {
        let i=0;
        if(doc._id !=null)
        {
            doc._id = new ObjectID();
        }
        i = i+1;
        collection.insertOne(doc);
    });
    client.close();   
});
